﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewGenAlg
{
    class Element
    {
        public int weight;
        public int price;
        public double wp;

        public Element(int weight, int price)
        {
            this.weight = weight;
            this.price = price;
            wp = price / weight;
        }
    }
}
