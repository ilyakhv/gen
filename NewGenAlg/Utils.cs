﻿using System.Collections.Generic;
using System.IO;

namespace NewGenAlg
{
    static class Utils
    {
        public static List<string> ReadFile(int file)
        {
            var sFile = "";
            var text = new List<string>();
            switch (file)
            {
                case 1:
                    sFile = "task_3_01_n5.txt";
                    break;
                case 2:
                    sFile = "task_3_02_n5.txt";
                    break;
                case 3:
                    sFile = "task_3_03_n10.txt";
                    break;
                case 4:
                    sFile = "task_3_04_n10.txt";
                    break;
                case 5:
                    sFile = "task_3_05_n50.txt";
                    break;
                case 6:
                    sFile = "task_3_06_n50.txt";
                    break;
                case 7:
                    sFile = "task_3_07_n100.txt";
                    break;
                case 8:
                    sFile = "task_3_08_n100.txt";
                    break;
                case 9:
                    sFile = "task_3_09_n1000.txt";
                    break;
                case 10:
                    sFile = "task_3_10_n1000.txt";
                    break;
            }

            using (StreamReader fs = new StreamReader(@"C:\Users\stand\Desktop\SpecSem\Task3\" + sFile))
            {
                while (true)
                {
                    string temp = fs.ReadLine();
                    if (temp == null) break;
                    text.Add(temp);
                }
            }

            return text;
        }
    }
}
