﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NewGenAlg
{
    class Program
    {
        public static List<string> text = new List<string>();
        public static int perf = 0;
        public static int number = 0;
        public static List<int> laborIntensity = new List<int>();
        public static List<int> profit = new List<int>();
        static void Main(string[] args)
        {
            Console.Write("Task: ");
            var numberTask = Console.Read();
            readFile(Convert.ToInt32(numberTask));
            read(text);
            var elements = new List<Element>();
            for (var i = 0; i < number; i++)
            {
                elements.Add(new Element(laborIntensity[i], profit[i]));
            }

            var individums = new List<Individum>();

            Console.Write("Number population: ");
            var numberPop = Convert.ToInt32(Console.Read());

        }

        public static void readFile(int file)
        {
            var sFile = "";
            switch (file)
            {
                case 1:
                    sFile = "task_3_01_n5.txt";
                    break;
                case 2:
                    sFile = "task_3_02_n5.txt";
                    break;
                case 3:
                    sFile = "task_3_03_n10.txt";
                    break;
                case 4:
                    sFile = "task_3_04_n10.txt";
                    break;
                case 5:
                    sFile = "task_3_05_n50.txt";
                    break;
                case 6:
                    sFile = "task_3_06_n50.txt";
                    break;
                case 7:
                    sFile = "task_3_07_n100.txt";
                    break;
                case 8:
                    sFile = "task_3_08_n100.txt";
                    break;
                case 9:
                    sFile = "task_3_09_n1000.txt";
                    break;
                case 10:
                    sFile = "task_3_10_n1000.txt";
                    break;
                default:
                    break;
            }

            text.Clear();

            using (StreamReader fs = new StreamReader(@"C:\Users\stand\Desktop\SpecSem\Task3\" + sFile))
            {
                while (true)
                {
                    string temp = fs.ReadLine();
                    if (temp == null) break;
                    text.Add(temp);
                }
            }
        }

        public static void read(List<string> text)
        {
            perf = int.Parse(text[0]);
            number = int.Parse(text[1]);

            laborIntensity = text[2].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                   .Select(n => int.Parse(n))
                   .ToList();

            profit = text[3].Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                   .Select(n => int.Parse(n))
                   .ToList();
        }
    }
}
