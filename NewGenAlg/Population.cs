﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NewGenAlg
{
    class Population
    {
        private List<Individum> individums;
        private int size;
        private int maxWeight;
        private List<Element> elements;
        private static Random rand = new Random();
        public Population(List<Individum> individums, int size, List<Element> elements, int maxWeight)
        {
            this.size = size;
            this.elements = elements;
            this.individums = individums;
            this.maxWeight = maxWeight;
        }

        public void Crossing()
        {
            int k = rand.Next(size);

            var descedants = new List<Individum>();

            int sizePop = individums.Count;

            for(int i = 0; i < sizePop; i++)
            {
                if (individums[i].IsWatching())
                {
                    var partner = individums.FirstOrDefault(it => it.IsWatching() && i != individums.IndexOf(it));
                    if (partner != null)
                    {
                        var firstChild = new Individum(individums[i].GetElements().GetRange(0, k), partner.GetElements().GetRange(k + 1, size), size);
                        var secondChild = new Individum(individums[i].GetElements().GetRange(k + 1, size), partner.GetElements().GetRange(0, k), size);
                        descedants.Add(firstChild);
                        descedants.Add(secondChild);
                        partner.UnWatch();
                    }
                    individums[i].UnWatch();
                }
            } 
        }

        public void Mutation()
        {
            foreach(var obj in individums)
            {
                obj.Mutation();
                var price = 0;
                var weight = 0;
                var el = obj.GetElements();
                for(int i = 0; i < el.Count; i++)
                {
                    if(el[i] == 1)
                    {
                        price += elements[i].price;
                        weight += elements[i].weight;
                    }
                }
                obj.SetPrice(price);
                obj.SetWeight(weight);
            }
        }

        public void Selection()
        {
            individums = individums.OrderByDescending(it => it.GetPrice()).Take(size).ToList();
        }
    }
}
