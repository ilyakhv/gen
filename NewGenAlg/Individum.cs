﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NewGenAlg
{
    class Individum
    {
        private List<int> individum;
        static Random rand = new Random();
        private int size;
        private bool watching;
        private int price = 0;
        private int weight = int.MaxValue;
        public Individum(int size)
        {
            this.size = size;
            watching = true;
            individum = new List<int>();
            for(int i = 0; i < size; i++)
            {
                individum.Add(rand.Next(2));
            }
        }

        public Individum(List<int> firstParent, List<int> secondParent, int size)
        {
            this.size = size;
            watching = true;
            individum = new List<int>();
            individum.AddRange(firstParent);
            individum.AddRange(secondParent);
        }

        public List<int> GetElements()
        {
            return individum;
        }

        public bool IsWatching()
        {
            return watching;
        }

        public void UnWatch()
        {
            watching = false;
        }

        public void Mutation()
        {
            for(int i = 0; i < size; i++)
            {
                if (individum[i] == 0)
                {
                    individum[i] = 1;
                }
                else
                {
                    individum[i] = 0;
                }
            }
        }

        public void SetPrice(int price)
        {
            this.price = price;
        }

        public void SetWeight(int weight)
        {
            this.weight = weight;
        }

        public int GetPrice()
        {
            return price;
        }
    }
}
